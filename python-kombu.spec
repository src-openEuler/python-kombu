%global _empty_manifest_terminate_build 0
Name:		python-kombu
Version:	5.4.0
Release:	1
Summary:	Messaging library for Python.
License:	BSD-3-Clause
URL:		https://github.com/celery/kombu
Source0:	https://files.pythonhosted.org/packages/b6/f4/d3e57b1c351bb47ce25b16e1cf6ea05df4613dbe56e3cf32ea80df1a8b4d/kombu-5.4.0.tar.gz
BuildArch:	noarch

Requires:	python3-amqp
Requires:	python3-importlib-metadata
Requires:	python3-azure-servicebus
Requires:	python3-azure-storage-queue
Requires:	python3-consul
Requires:	python3-librabbitmq
Requires:	python3-pymongo
Requires:	python3-msgpack
Requires:	python3-pyro4
Requires:	python3-qpid-python
Requires:	python3-qpid-tools
Requires:	python3-redis
Requires:	python3-softlayer-messaging
Requires:	python3-sqlalchemy
Requires:	python3-boto3
Requires:	python3-pycurl
Requires:	python3-PyYAML
Requires:	python3-kazoo

%description
Kombu is a messaging library for Python.

The aim of Kombu is to make messaging in Python as easy as possible by providing an idiomatic high-level interface for the AMQ protocol, and also provide proven and tested solutions to common messaging problems.

AMQP is the Advanced Message Queuing Protocol, an open standard protocol for message orientation, queuing, routing, reliability and security, for which the RabbitMQ messaging server is the most popular implementation.

%package -n python3-kombu
Summary:	Messaging library for Python.
Provides:	python-kombu
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-kombu

Kombu is a messaging library for Python.

The aim of Kombu is to make messaging in Python as easy as possible by providing an idiomatic high-level interface for the AMQ protocol, and also provide proven and tested solutions to common messaging problems.

AMQP is the Advanced Message Queuing Protocol, an open standard protocol for message orientation, queuing, routing, reliability and security, for which the RabbitMQ messaging server is the most popular implementation.

%package help
Summary:	Development documents and examples for kombu
Provides:	python3-kombu-doc
%description help
Kombu is a messaging library for Python.

The aim of Kombu is to make messaging in Python as easy as possible by providing an idiomatic high-level interface for the AMQ protocol, and also provide proven and tested solutions to common messaging problems.

AMQP is the Advanced Message Queuing Protocol, an open standard protocol for message orientation, queuing, routing, reliability and security, for which the RabbitMQ messaging server is the most popular implementation.

%prep
%autosetup -n kombu-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-kombu -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Aug 19 2024 liyue01 <liyue01@kylinos.cn> - 5.4.0-1
- Update package to version 5.4.0
- Fix: Fanout exchange messages mixed across virtual databases in Redis sentinel
- enhance: allow uses to disable broker heartbeats by not providing a timeout
- Fix Redis connections after reconnect - consumer starts consuming the tasks after crash 



* Tue Jul 23 2024 lilu <lilu@kylinos.cn> - 5.3.7-1
- Update package to version 5.3.7
  Fix: Bumpversion commit wasn’t pushed to main correctly for v5.3.6

* Thu Feb 22 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 5.3.5-1
- Update package to version 5.3.5
  Fix crash when using global_keyprefix with a sentinel connection

* Fri Jun 30 2023 zixuanchen <chenzixuan@kylinos.cn> - 5.3.1-1
- Update to 5.3.1

* Mon Aug 8 2022 wenzhiwei <wenzhiwei@kylinos.cn> - 5.2.4-1
- Update to 5.2.4

* Thu Jun 16 2022 OpenStack_SIG <openstack@openeuler.org> - 5.2.3-1
- Upgrade version for openstack yoga

* Wed May 11 2022 houyingchao <houyingchao@h-partners.com> - 5.0.2-2
- License compliance rectification

* Mon Dec 28 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
